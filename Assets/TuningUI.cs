﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TuningUI : MonoBehaviour
{
    public void SetRadiator(int num)
    {
        GameManager.Instance.TuningSystem.UpdateRadiator(num);
    }
    public void SetSpoiler(int num)
    {
        GameManager.Instance.TuningSystem.UpdateSpoiler(num);
    }
    public void SetDoorsteps(int num)
    {
        GameManager.Instance.TuningSystem.UpdateDoorstep(num);
    }
}
