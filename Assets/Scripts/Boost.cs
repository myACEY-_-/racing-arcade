﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boost : MonoBehaviour
{
    public float force = 100;
    Rigidbody _rb;
    
    private void OnTriggerEnter(Collider other)
    {
        Rigidbody rb = other.GetComponent<Rigidbody>();
        if (rb)
        {
            _rb = rb;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Rigidbody rb = other.GetComponent<Rigidbody>();
        if (rb)
        {
            _rb = null;
        }
    }
    public void FixedUpdate()
    {
        if (_rb != null)
            _rb.AddForce(_rb.transform.forward * force, ForceMode.Impulse);

    }
}
