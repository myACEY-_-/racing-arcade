﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public GameObject finishLine;
    public bool isFinish = false;
    public List<ParticleSystem> fires = new List<ParticleSystem>();

    private void Awake()
    {
        if (isFinish)
        {
            GameObject finish = Instantiate(finishLine, transform.position + Vector3.up * 0.001f, Quaternion.Euler(-90, 0, 0));
            for(int i = 0; i < fires.Count; i++)
            {
                fires[i].transform.localScale = new Vector3(5, 5, 5);
            }
        }
    }
}
