﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    Animator anim;

    private void Start()
    {
        Cursor.visible = true;
        anim = GetComponent<Animator>();
        Time.timeScale = 1;
        RecordsSystem.LoadRecords();
    }
    public void OpenScene()
    {
        SceneManager.LoadScene(1);
    }

    public void OpenRecords()
    {
        anim.SetBool("recordsOpen", !anim.GetBool("recordsOpen"));
    }

    public void ExitApp()
    {
        Application.Quit();
        print("EXIT");
    }
}
