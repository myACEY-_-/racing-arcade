﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
public class Car : MonoBehaviour
{
    public float curSpeed;
    public Transform centerOfMass;
    public float motorTorque = 1500f;
    public float maxSteer = 20f;
    public float breakForce;
    public float Steer { get; set; }
    public float Throttle { get; set; }
    private Rigidbody _rb;

    private Wheels[] wheels;

    bool tireMarksFlag;
    public bool brake;
    public bool backInput;

    public Light[] backLights;
    bool isGrounded = false;
    /// <summary>
    /// 1-radiator 2-spoiler 3-doorsteps
    /// </summary>
    public List<TuningElement> TElements = new List<TuningElement>();

    private void Start()
    {
        _rb = GetComponentInParent<Rigidbody>();
        _rb.centerOfMass = centerOfMass.localPosition;
        wheels = GetComponentsInChildren<Wheels>();

        foreach (Light light in backLights)
        {
            light.enabled = false;
        }
    }

    private void Update()
    {
        

        #region WheelControll
        foreach (var wheel in wheels)
        {
            wheel.SteerAngle = Steer * maxSteer;
            
            if (!brake)
            {
                wheel.Torque = Throttle * motorTorque;
                wheel.power = true;
                wheel._wheelColl.brakeTorque = 0;
            }
            else
            {
                wheel.power = false;
                wheel._wheelColl.brakeTorque = breakForce;
            }
        }
        #endregion


        #region lights
        if (backInput || brake)
        {
            foreach (Light light in backLights)
            {
                light.enabled = true;
            }
        }
        else
        {
            foreach (Light light in backLights)
            {
                light.enabled = false;
            }
        }
        #endregion

        curSpeed = _rb.velocity.magnitude * 3.6f * 2.237f;
        isGrounded = isGrouned();
        #region Drift
        if (!isGrounded)
            return;
        if (!brake)
        {
            if ((curSpeed > 100 && !Input.GetKey(KeyCode.S)) || (curSpeed <= 50 && !Input.GetKey(KeyCode.W))) //StopsEmitter
            {
                StopEmitter();
            }
            else if (curSpeed <= 100 && Input.GetKey(KeyCode.W)) //Smoke
            {
                StartEmitter(false, true);
            }
        }
        else
        {
            StartEmitter(true, true);
        }
        #endregion
    }

    void StartEmitter(bool showTireMark, bool showTireSmoke)
    {
        
        if (tireMarksFlag) return;
        foreach (var wheel in wheels)
        {
            if (showTireMark)
                wheel.tireMark.emitting = true; 
            if(showTireSmoke && wheel.tireType == TireType.back)
                wheel.tireSmoke.Play();
        }
        tireMarksFlag = true;
    }
    void StopEmitter()
    {

        if (!tireMarksFlag) return;
        foreach (var wheel in wheels)
        {
            wheel.tireMark.emitting = false;
            wheel.tireSmoke.Stop();
        }
        tireMarksFlag = false;
    }
    bool isGrouned()
    {
        return Physics.Raycast(transform.position + Vector3.up * 0.2f, -Vector3.up, 0.25f);
    }
    public void UpdateBoosts()
    {

        IDamageable id = GetComponentInParent<IDamageable>();
        if (!id) return;
        foreach (TuningElement te in TElements)
        {
            id.curHp += te.HealthBoost;
            id.maxHP += te.HealthBoost;

            id.curArmor += te.ArmorBoost;
            id.maxArmor += te.HealthBoost;

            motorTorque += te.SpeedBoost;
        }
    }
}
