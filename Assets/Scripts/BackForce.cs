﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class BackForce : MonoBehaviour
{
    Transform pos;
    Rigidbody rb;
    Transform target;
    private void Start()
    {
        target = GetComponent<CarAIControl>().m_Target;
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 11)
        {
            pos = transform;
            StartCoroutine("startWaiting");
        }
    }

    IEnumerator startWaiting()
    {
        yield return new WaitForSeconds(3);
        if (pos == transform)
        {
            GoBack();
        }
        else if (pos != transform)
            print("NO HELP!");
    }

    void GoBack()
    {
        rb.AddRelativeForce(-Vector3.forward * 100, ForceMode.Impulse);
    }
}
