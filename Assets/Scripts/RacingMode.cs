﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RacingMode : MonoBehaviour
{
    public float BestLapTime { get; private set; } = Mathf.Infinity;
    public float LastLapTime { get; private set; } = 0;
    public float CurrentLapTime { get; private set; } = 0;
    public float CurrentLap { get; private set; } = 0;

    RacingUIController racingUIController;

    #region lapTime
    [Header("Lap Time")]
    public GameObject RacePanel;
    private float lapTimerTimstamp;
    private int lastCheckpointPast;
    private Transform checkpointsParent;
    private int checkpointCount;
    public List<GameObject> checkpoints = new List<GameObject>();
    private int checkpointLayer;
    #endregion

    #region carRecover
    [Header("Car Recover")]
    public float recoverTime = 5;
    float timer = 0;
    float maxTimerValue = 3;
    bool canRecover = true;
    public GameObject recoverPanel;
    Slider recoverSlider;
    #endregion


    private void Start()
    {
        RecordsSystem.LoadRecords();
        checkpointsParent = GameObject.Find("Checkpoints").transform;
        checkpointCount = checkpointsParent.childCount;
        checkpointLayer = LayerMask.NameToLayer("Checkpoint");
        RenameCheckpoints();
        recoverSlider = recoverPanel.GetComponentInChildren<Slider>();
        recoverSlider.maxValue = maxTimerValue;
        recoverPanel.SetActive(false);
        racingUIController = FindObjectOfType<RacingUIController>();
    }
    private void Update()
    {
        CurrentLapTime = lapTimerTimstamp > 0 ? Time.time - lapTimerTimstamp : 0;
        if (timer >= 3)
        {
            print("+");
            timer = 0;
            canRecover = false;
            StartCoroutine("RecoverUpdate");
            recoverPanel.SetActive(false);
            RecoverCar();
        }
        recoverSlider.value = timer;

        if (Input.GetKey(KeyCode.T) && canRecover)
        {
            timer += Time.deltaTime;
            recoverPanel.SetActive(true);
        }
        if (!Input.GetKey(KeyCode.T))
        {
            if (timer < maxTimerValue && timer > 0)
            {
                timer -= Time.deltaTime;
            }
            if (timer <= 0)
                recoverPanel.SetActive(false);
        }
    }
    #region checkpoints
    IEnumerator RecoverUpdate()
    {
        yield return new WaitForSeconds(recoverTime);
        canRecover = true;
    }

    void RenameCheckpoints()
    {
        int i = 0;
        foreach (Transform checkpoint in checkpointsParent.transform)
        {
            i += 1;
            checkpoint.name = i.ToString();
            checkpoints.Add(checkpoint.gameObject);
        }
    }
    void StartLap()
    {
        CurrentLap++;
        lastCheckpointPast = 1;
        lapTimerTimstamp = Time.time;
        print("New Lap!");
    }

    void EndLap()
    {
        LastLapTime = Time.time - lapTimerTimstamp;
        BestLapTime = Mathf.Min(LastLapTime, BestLapTime);

        RecordsSystem.UpdateRecords(LastLapTime);
        racingUIController.UpdateBestLapTime();
        
    }
    void RecoverCar()
    {
        transform.position = new Vector3(checkpoints[lastCheckpointPast - 1].transform.position.x, checkpoints[lastCheckpointPast - 1].transform.position.y * 2, checkpoints[lastCheckpointPast - 1].transform.position.z);
        Vector3 eulerRotation = new Vector3(0, checkpoints[lastCheckpointPast - 1].transform.eulerAngles.y, 0);

        transform.rotation = Quaternion.Euler(eulerRotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != checkpointLayer) return;
        if (other.name == "1")
        {
            if (lastCheckpointPast == checkpointCount)
            {
                EndLap();
            }

            //If it is First lap or passes last checkpoint- start a new lap
            if (CurrentLap == 0 || lastCheckpointPast == checkpointCount)
            {
                StartLap();
            }
            return;
        }

        if (other.gameObject.name == (lastCheckpointPast + 1).ToString())
        {
            lastCheckpointPast++;
        }

    }
    #endregion
}
