﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheels : MonoBehaviour
{
    public TireType tireType;

    public bool steer;
    public bool invertSteer;
    public bool power;

    public float SteerAngle { get; set; }
    public float Torque { get; set; }

    public WheelCollider _wheelColl;
    private Transform _wheelTransform;
    [HideInInspector] public TrailRenderer tireMark;
    [HideInInspector] public ParticleSystem tireSmoke;

    private void Start()
    {
        //_wheelColl = GetComponentInChildren<WheelCollider>();
        _wheelTransform = GetComponentInChildren<MeshRenderer>().GetComponent<Transform>();
        tireMark = _wheelColl.GetComponentInChildren<TrailRenderer>();
        tireSmoke = _wheelColl.GetComponentInChildren<ParticleSystem>();
    }

    private void Update()
    {
        _wheelColl.GetWorldPose(out Vector3 pos, out Quaternion rot);
        _wheelTransform.position = pos;
        _wheelTransform.rotation = rot;
    }

    private void FixedUpdate()
    {
        if (steer)
        {
            _wheelColl.steerAngle = SteerAngle * (invertSteer ? -1 : 1);
        }

        if (power)
        {
            _wheelColl.motorTorque = Torque;
        }
    }

}

public enum TireType { front, back}
