﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDamageable : MonoBehaviour
{
    [Header ("Health And Armor")]
    public float DamageMultiplier = 1;
    [SerializeField] public float curHp;
    public float maxHP;
    [SerializeField] public float curArmor;
    public float maxArmor;
    public virtual void TakeDamage(float damage)
    {
        if (damage < 10)
            return;
        if (curArmor > 0)
        {
            if (curArmor - damage < 0)
            {
                float a = damage - curArmor;
                curHp -= a;
                curArmor = 0;
            }
            else
            {
                curArmor -= damage;
            }
        }
        else
        {
            curHp -= damage;
        }
        if (curHp <= 0)
        {
            DestroyCar();
        }

    }

    public virtual void DestroyCar()
    {
        //
    }

    private void OnCollisionEnter(Collision collision)
    {
        IDamageable other = collision.gameObject.GetComponent<IDamageable>();
        if (other)
        {
            float damage = collision.gameObject.GetComponent<IDamageable>().DamageMultiplier * collision.relativeVelocity.magnitude;
            other.TakeDamage(damage);
            float damage2 = GetComponent<IDamageable>().DamageMultiplier * collision.relativeVelocity.magnitude;
            TakeDamage(damage2);
        }
        else if(collision.gameObject.layer == 11)
        {
            float damage2 = collision.relativeVelocity.magnitude * 0.2f;
            TakeDamage(damage2);
        }
    }
}
