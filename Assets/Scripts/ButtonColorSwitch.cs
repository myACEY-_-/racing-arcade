﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ButtonColorSwitch : MonoBehaviour
{
    public TextMeshProUGUI text;
    Color baseColor;
    private void Start()
    {
        baseColor = text.color;
    }
    public void Enter()
    {
        text.color = new Color(1, 1, 1);
    }
    public void Exit()
    {
        text.color = baseColor;
    }
    public void Click()
    {
        text.color = new Color(0.83f, 0.83f, 0.83f);
    }
}
