﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameMode { Racing, PoliceChasing }
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public InputController inputController { get; private set; }
    
    public GameMode gameMode;
    public TuningSystem TuningSystem;

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        inputController = GetComponentInChildren<InputController>();
        Cursor.visible = false;

    }
}
