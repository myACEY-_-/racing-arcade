﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    public bool LookAt = false;

    private void FixedUpdate()
    {
        Vector3 desiredPos = target.position + offset;
        Vector3 smoothPos = Vector3.Lerp(transform.position, desiredPos, smoothSpeed * Time.deltaTime);
        transform.position = smoothPos;
        
        if(LookAt) transform.LookAt(target);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            LookAt = !LookAt;
            if (LookAt)
                smoothSpeed = 0.3f;
            else
            {
                smoothSpeed = 9f;
                Vector3 eulerRotation = new Vector3(62, 180, 0);

                transform.rotation = Quaternion.Euler(eulerRotation);
            }
        }
    }
}
