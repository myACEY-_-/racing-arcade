﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
using System.Linq;

public class RecordsSystem : MonoBehaviour
{
    private static List<float> highScores = new List<float>();
    public GameObject noRecords;
    private void Awake()
    {
        LoadRecords();
    }
    private void OnEnable()
    {
        if (highScores.Count > 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (i < highScores.Count)
                {
                    transform.GetChild(i).GetChild(1).GetComponent<TextMeshProUGUI>().text = $"{(int)highScores[i] / 60}:{highScores[i] % 60:00}";
                    transform.GetChild(i).gameObject.SetActive(true);
                }
                else
                    transform.GetChild(i).gameObject.SetActive(false);
            }
            noRecords.SetActive(false);
        }
        else
        {
            noRecords.SetActive(true);
        }
    }

    public static void SaveRecords()
    {
        string path = Application.dataPath + "/records.txt";
        if (File.Exists(path))
        {
            List<string> tmp = new List<string>();
            foreach (float score in highScores)
            {
                tmp.Add(score.ToString());
            }
            File.WriteAllLines(path, tmp);
        }
        else
        {
            Debug.LogError("Save file not found in" + path);
        }
    }

    public static List<float> LoadRecords()
    {
        //print("LoadRecords");
        highScores = new List<float>();

        string path = Application.dataPath + "/records.txt";
        if (File.Exists(path))
        {

            string[] tmp = File.ReadAllLines(path);
            foreach (string rec in tmp)
            {
                highScores.Add(float.Parse(rec));
            }
            highScores.Sort();
        }
        else
        {
            Debug.LogError("Save file not found in" + path);
        }
        return highScores;
    }

    public static void UpdateRecords(float record)
    {
        if (highScores.Count >= 7) 
        { 
            if (record < highScores.Max())
            {
                highScores.Add(record);
                highScores.Sort();
                highScores = highScores.GetRange(0, Mathf.Min(7, highScores.Count));
            }
        }
        else
        {
            highScores.Add(record);
            highScores.Sort();
        }
        SaveRecords();
    }
}
