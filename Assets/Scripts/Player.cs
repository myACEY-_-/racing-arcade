﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using System.Linq;

public class Player : MonoBehaviour
{
    public enum ControlType { HumanInput, AI}

    #region tip
    [Header("Tip")]
    public GameObject tip;
    #endregion
    #region escapeMenu
    [Header("Escape Menu")]
    public GameObject EscapeMenu;
    #endregion
    private Car carController;
    // Start is called before the first frame update
    void Awake()
    {
        // gameMode = GameManager.Instance.gameMode;
        //RecordsSystem.LoadRecords();
        carController = GetComponentInChildren<Car>();
        EscapeMenu.SetActive(false);
        Time.timeScale = 1;
    }
    void Update()
    {
        #region Inputs
        carController.Steer = GameManager.Instance.inputController.SteerInput;
        carController.Throttle = GameManager.Instance.inputController.ThrottleInput;
        carController.brake = GameManager.Instance.inputController.DragInput;
        carController.backInput = GameManager.Instance.inputController.BackInput;

            
        if (Input.GetKeyDown(KeyCode.V))
        {
            tip.SetActive(!tip.activeSelf);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            EscapeMenuActivate();
        }
        #endregion
    }

    public void EscapeMenuActivate()
    {
        EscapeMenu.SetActive(!EscapeMenu.activeSelf);
        Cursor.visible = !Cursor.visible;
        tip.SetActive(!tip.activeSelf);
        //RacePanel.SetActive(!RacePanel.activeSelf);
        //CarUI.SetActive(!CarUI.activeSelf);
        if (EscapeMenu.activeSelf)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    /*public void LoadPlayer()
    {
        RecordsSystem.LoadRecords();
    }*/
}
