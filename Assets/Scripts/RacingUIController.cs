﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RacingUIController : MonoBehaviour
{
    public GameObject UIPanel;
    public TextMeshProUGUI UITextCurrentLapAndTime;
    public TextMeshProUGUI UITextBestTime;

    public RacingMode UpdateUIForPlayer;

    private float currentLap;
    private float currentLapTime;

    public float bestLapTime;
    bool hasBestLap = false;
    private void Start()
    {
        if (RecordsSystem.LoadRecords().Count > 0) 
        {
            bestLapTime = RecordsSystem.LoadRecords()[0];
            hasBestLap = true;
        }
        else
        {
            hasBestLap = false;
        }

    }
    public void Update()
    {
        if (UpdateUIForPlayer == null)
        {
            print("no player!");
            return;
        }

        if (currentLapTime != UpdateUIForPlayer.CurrentLapTime || UpdateUIForPlayer.CurrentLap != currentLap)
        {
            currentLap = UpdateUIForPlayer.CurrentLap;
            currentLapTime = UpdateUIForPlayer.CurrentLapTime;
            UITextCurrentLapAndTime.text = $"{(int)currentLap}/{(int)currentLapTime / 60}:{(currentLapTime) % 60:00} ";

            /*if (UpdateUIForPlayer.BestLapTime != Mathf.Infinity)
            {
                bestLapTime = UpdateUIForPlayer.BestLapTime;
                UITextBestTime.text = $"Best:{(int)bestLapTime / 60}:{(bestLapTime) % 60:00}";
            }
            else
                UITextBestTime.text = $"Best:NONE";*/
            if (hasBestLap)
                UITextBestTime.text = $"BEST:{(int)bestLapTime / 60}:{(bestLapTime) % 60:00}";
            else
                UITextBestTime.text = $"BEST:NONE";
        }

    }
    public void UpdateBestLapTime()
    {
        print("+");
        bestLapTime = RecordsSystem.LoadRecords()[0];
        hasBestLap = true;
    }
}
