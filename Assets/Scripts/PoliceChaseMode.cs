﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PoliceChaseMode : IDamageable
{
    #region health and armor
    //public GameObject CarUI;
    public Slider healthSlider;
    public Slider armorSlider;
    #endregion
    [Header("Police Chase")]
    Animator worldCanvas;
    public GameObject DestoryedMenu;
    private Car carController;

    public float arestTimer = 0;
    public int policeCars = 0;
    public bool arest = false;
    public float TimeToArest = 10;

    public TextMeshProUGUI label;
    public Slider arestSlider;


    // Start is called before the first frame update
    void Start()
    {
        worldCanvas = FindObjectOfType<CanvasScaler>().GetComponent<Animator>();
        curHp = maxHP;
        curArmor = maxArmor;

        healthSlider.maxValue = maxHP;
        armorSlider.maxValue = maxArmor;

        healthSlider.value = maxHP;
        armorSlider.value = maxArmor;
        carController = GetComponentInChildren<Car>();
        arestSlider.maxValue = TimeToArest;
        arestSlider.value = 0;
    }
    private void FixedUpdate()
    {
        arestSlider.value = arestTimer;
        if (arestSlider.value == 0)
        {
            arestSlider.gameObject.SetActive(false);
        }
        else
            arestSlider.gameObject.SetActive(true);
        if (arest && carController.curSpeed < 50)
        {
            arestTimer += Time.deltaTime;
            if (arestTimer >= TimeToArest)
                Losing(true);
        }
        else if(!arest && arestTimer > 0)
        {
            arestTimer -= Time.deltaTime;
        }
    }


    /*private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.layer == 11)
        {
            print("+");
            float damage = GetComponent<IDamageable>().DamageMultiplier * collision.relativeVelocity.magnitude;
            if (damage > 25)
                TakeDamage(damage);
        }
    }*/
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 13)
        {
            policeCars++;
            arest = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 13)
        {
            policeCars--;
            if (policeCars == 0)
                arest = false;
        }
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);

        healthSlider.value = curHp;
        armorSlider.value = curArmor;
    }

    public override void DestroyCar()
    {
        base.DestroyCar();
        Losing(false);
    }

    public void Losing(bool arested)
    {
        worldCanvas.SetTrigger("destoryed");
        if (!arested)
            label.text = "DESTROYED";
        else
            label.text = "ARESTED";

        //StartCoroutine("TimeStopEffect");
        Cursor.visible = true;
        Time.timeScale = 0;
    }

    IEnumerator TimeStopEffect()
    {
        float tiempoOriginal = Time.timeScale;
        Time.timeScale = 0;
        float t = 1f;

        while (t > 0f)
        {
            t -= Time.unscaledDeltaTime;

            yield return null;
        }

        Time.timeScale = tiempoOriginal;
    }
}
