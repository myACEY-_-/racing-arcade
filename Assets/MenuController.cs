﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class MenuController : MonoBehaviour
{
    TuningSystem ts;
    Car carController;
    public int selectedMode = 1;
    public GameObject mainMenu;
    public GameObject modeSelect;
    public GameObject recordsMenu;
    public GameObject tuningMenu;
    public TextMeshProUGUI modeText;
    IDamageable idam;

    [Header("Health")]
    public Slider healthSliderMain;
    public Slider healthSlider;

    [Header("Armor")]
    public Slider armorSliderMain;
    public Slider armorSlider;

    [Header("Speed")]
    public Slider speedSliderMain;
    public Slider speedSlider;
    private void Start()
    {
        carController = FindObjectOfType<Car>();
        ts = FindObjectOfType<TuningSystem>();
        Time.timeScale = 1f;
        Cursor.visible = true;
        idam = FindObjectOfType<IDamageable>();

        healthSliderMain.maxValue = idam.maxHP + 1000;
        healthSlider.maxValue = idam.maxHP + 1000;
        healthSliderMain.value = idam.maxHP;
        healthSlider.value = idam.maxHP;

        armorSliderMain.maxValue = idam.maxArmor + 1000;
        armorSlider.maxValue = idam.maxArmor + 1000;
        armorSliderMain.value = idam.maxArmor;
        armorSlider.value = idam.maxArmor;

        speedSliderMain.maxValue = carController.motorTorque + 1000;
        speedSlider.maxValue = carController.motorTorque + 1000;
        speedSliderMain.value = carController.motorTorque;
        speedSlider.value = carController.motorTorque;
    }
    #region UI
    #region main
    public void StartGame()
    {
        SceneManager.LoadScene(selectedMode);
    }
    public void OpenModeSelector()
    {
        modeSelect.SetActive(!modeSelect.activeSelf);
    }
    public void SelectMode(int a)
    {
        selectedMode = a;
        modeSelect.SetActive(!modeSelect.activeSelf);
        if (a == 1)
            modeText.text = "RACING";
        else
            modeText.text = "POLICE CHASE";
    }
    public void OpenRecords()
    {
        recordsMenu.SetActive(!recordsMenu.activeSelf);
    }
    
    public void OpenTuningMenu()
    {
        tuningMenu.SetActive(!tuningMenu.activeSelf);
        mainMenu.SetActive(!mainMenu.activeSelf);
    }
    #endregion
    #region tuning
    public void SetRadiator(int num)
    {
        ts.UpdateRadiator(num);
    }

    public void SetSpoiler(int num)
    {
        ts.UpdateSpoiler(num);
    }

    public void SetDoorsteps(int num)
    {
        ts.UpdateDoorstep(num);
    }
    #endregion

    #endregion
}
