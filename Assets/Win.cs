﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    public Animator canvas;

    private void OnTriggerEnter(Collider other)
    {
        Player pl = other.GetComponent<Player>();
        if (pl)
        {
            Time.timeScale = 0.5f;
            canvas.SetTrigger("win");
        }
    }
}
