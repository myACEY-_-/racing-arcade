﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Globalization;

public class TuningSaveSystem : MonoBehaviour
{
    private static List<int> Indexes = new List<int>();
    public static void SaveTuning()
    {
        string path = Application.dataPath + "/tuning.txt";
        if (File.Exists(path))
        {
            List<string> tmp = new List<string>();
            foreach (float index in Indexes)
            {
                tmp.Add(index.ToString());
            }
            File.WriteAllLines(path, tmp);
        }
        else
        {
            Debug.LogError("Save file not found in" + path);
        }
    }

    public static List<int> LoadTuning()
    {
        //print("LoadRecords");
        Indexes = new List<int>();

        string path = Application.dataPath + "/tuning.txt";
        if (File.Exists(path))
        {

            string[] tmp = File.ReadAllLines(path);
            foreach (string rec in tmp)
            {
                Indexes.Add(int.Parse(rec));
            }
        }
        else
        {
            Debug.LogError("Save file not found in" + path);
        }
        return Indexes;
    }

    public static void UpdateTuningIndexes(List<int> indexes)
    {
        Indexes = indexes;
        SaveTuning();
    }
}
