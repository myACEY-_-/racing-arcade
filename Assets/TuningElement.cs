﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuningElement : MonoBehaviour
{
    public float HealthBoost;
    public float ArmorBoost;
    public float SpeedBoost;
}
