﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuningSystem : MonoBehaviour
{
    [Header("Radiator")]
    public List<TuningElement> radiators = new List<TuningElement>();
    [Header("Spoilers")]
    public List<TuningElement> spoilers = new List<TuningElement>();
    [Header("Doorstep")]
    public List<TuningElement> doorsteps = new List<TuningElement>();
    Car carController;
    /// <summary>
    /// 1-radiator 2-spoiler 3-doorsteps
    /// </summary>
    List<int> indexes = new List<int>();
    public void Awake()
    {
        //if(GameManager.Instance.gameObject != null)
            //GameManager.Instance.TuningSystem = this;
        /*for (int i = 0; i < 3; i++)
        {
            indexes.Add(0);
        }*/
        indexes = TuningSaveSystem.LoadTuning();
        carController = FindObjectOfType<Car>();

        carController.TElements[0] = Instantiate(radiators[indexes[0]], carController.transform.position, carController.transform.rotation, carController.transform);
        carController.TElements[1] = Instantiate(spoilers[indexes[1]], carController.transform.position, carController.transform.rotation, carController.transform);
        carController.TElements[2] = Instantiate(doorsteps[indexes[2]], carController.transform.position, carController.transform.rotation, carController.transform);

        carController.UpdateBoosts();
    }

    public void UpdateRadiator(int num)
    {
        Destroy(carController.TElements[0].gameObject);
        carController.TElements[0] = Instantiate(radiators[num], carController.transform.position, carController.transform.rotation, carController.transform);
        indexes[0] = num;
        TuningSaveSystem.UpdateTuningIndexes(indexes);
        carController.UpdateBoosts();
    }

    public void UpdateSpoiler(int num)
    {
        Destroy(carController.TElements[1].gameObject);
        carController.TElements[1] = Instantiate(spoilers[num], carController.transform.position, carController.transform.rotation, carController.transform);
        indexes[1] = num;
        TuningSaveSystem.UpdateTuningIndexes(indexes);
        carController.UpdateBoosts();
    }

    public void UpdateDoorstep(int num)
    {
        Destroy(carController.TElements[2].gameObject);
        carController.TElements[2] = Instantiate(doorsteps[num], carController.transform.position, carController.transform.rotation, carController.transform);
        indexes[2] = num;
        TuningSaveSystem.UpdateTuningIndexes(indexes);
        carController.UpdateBoosts();
    }


}
