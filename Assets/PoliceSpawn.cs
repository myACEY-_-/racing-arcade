﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class PoliceSpawn : MonoBehaviour
{
    public CarAIControl policePrebab;
    public Transform spawnPoint;
    bool a = false;

    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p && a == false)
        {
            a = true;
            print(p);
            CarAIControl carAi = Instantiate(policePrebab, spawnPoint.position, spawnPoint.rotation);
            carAi.SetTarget(p.transform);
            Destroy(gameObject);
        }
    }
}
